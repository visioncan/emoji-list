List of iOS Emoji
=================

This is a listing of all the iOS emoji glyphs, and a Python script
which makes that possible.

The Emoji characters are displayed in the MacOS app CharacterPalette
app. The script `convert.py` is (was) used to extract the list of
emoji and their descriptions therefrom.

The [result of all this](http://www.grumdrig.com/emoji-list/) 
is only satisfactorily viewable from Safari.

